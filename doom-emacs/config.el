;; Doom emacs config

;;; Code:

(define-key evil-motion-state-map "k" 'evil-backward-char)
(define-key evil-motion-state-map "j" 'evil-forward-char)
(define-key evil-motion-state-map "l" 'evil-next-line)
(define-key evil-motion-state-map "h" 'evil-previous-line)

(provide 'config)
;;; config.el ends here
