# The following lines were added by compinstall

source ~/.shrc.user
[[ -e ~/.shrc.personal ]] && source ~/.shrc.personal

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'r:|[._-]=** r:|=**'
zstyle :compinstall filename '~/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_history
HISTSIZE=100000
SAVEHIST=100000
setopt APPEND_HISTORY
bindkey -e
# End of lines configured by zsh-newuser-install

# config for backword deleting a word
WORDCHARS=${WORDCHARS/\//}  # remove '/' from word chars.
# WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'

#PS1='%n@%m %c$ '
#PS1='%n %c%% '
PS1='%c%% '

# --------------- oh-my-zsh --------------------
source ~/.oh.my.zshrc

# HSTR configuration - add this to ~/.bashrc
alias hh=hstr                    # hh to be alias for hstr
export HISTFILE=~/.zsh_history  # ensure history file visibility
export HSTR_CONFIG=hicolor        # get more colors
#bindkey -s "\C-r" "\eqhstr\n"     # bind hstr to Ctrl-r (for Vi mode check doc)
#bindkey -s "\C-r" "\eqfzf\n"     # bind hstr to Ctrl-r (for Vi mode check doc)
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh


