noremap h k
noremap l j
noremap k h
noremap j l
noremap <space> :

" noremap t f
" noremap f t

nmap _ "
vmap _ "
nmap - '
vmap - '

nmap ]f :cnext
nmap [f :cprevious

" This unsets the "last search pattern" register by hitting return
nnoremap <silent> <CR> :nohlsearch<CR><CR>

" navigate tabs and panels
" nnoremap <C-t>     :tabnew<CR>
" inoremap <C-t>     <Esc>:tabnew<CR>
" move to the previous/next tabpage.
nnoremap <C-k> :tabprevious<CR>
nnoremap <C-j> :tabnext<CR>
nnoremap <S-Tab> :tabnext<CR>
vnoremap <C-k> <Esc>:tabprevious<CR>
vnoremap <C-j> <Esc>:tabnext<CR>
vnoremap <S-Tab> <Esc>:tabnext<CR>
inoremap <C-k> <Esc>:tabprevious<CR>
inoremap <C-j> <Esc>:tabnext<CR>
inoremap <S-Tab> <Esc>:tabnext<CR>

" Go to last active tab
au TabLeave * let g:lasttab = tabpagenr()
nnoremap <silent> <C-l> :exe "tabn ".g:lasttab<cr>
vnoremap <silent> <C-l> <Esc>:exe "tabn ".g:lasttab<cr>
inoremap <silent> <C-l> <Esc>:exe "tabn ".g:lasttab<cr>

nnoremap <C-s> <C-w>w
vnoremap <C-s> <C-w>w
inoremap <C-s> <Esc><C-w>w

nnoremap <C-w>F <C-w>vgf
vnoremap <C-w>F <C-w>vgf
nnoremap <C-w>yt :tab split<CR>
vnoremap <C-w>yt :tab split<CR>

" nnoremap <C-w>vf <C-w>vgf

tnoremap <Esc> <C-\><C-n>

