silent! call plug#begin('~/.vim/plugged')

Plug 'dracula/vim'
Plug 'mhinz/vim-startify'
Plug 'airblade/vim-rooter'

Plug 'tpope/vim-fugitive'
Plug 'previm/previm'

Plug 'vmchale/dhall-vim'

Plug 'DingDean/wgsl.vim'

call plug#end()

if (has("termguicolors"))
    set termguicolors
endif
syntax enable
" colorscheme evening
"colorscheme dracula

" ------------- nerdtree --------------
"nnoremap <leader>n :NERDTreeFocus<CR>
""nnoremap <C-n> :NERDTree<CR>
"nnoremap <leader>T :NERDTreeToggle<CR>
"nnoremap <leader>F :NERDTreeFind<CR>

"" -------------- fzf ----------------
"nnoremap <C-p> :GFiles<Cr>
""map <C-f> :Files<CR>
"nnoremap <leader>f :Files<CR>
"nnoremap <leader>b :Buffers<CR>
""nnoremap <leader>g :Rg<CR>
"nnoremap <leader>t :Tags<CR>
"nnoremap <leader>m :Marks<CR>

"let g:fzf_tags_command = 'ctags -R'

""let $FZF_DEFAULT_OPTS = '--layout=reverse --info=inline --bind=ctrl-p:up,ctrl-n:down'
"let $FZF_DEFAULT_OPTS = '--info=inline --bind=ctrl-p:up,ctrl-n:down'
""let $FZF_DEFAULT_COMMAND="rg --files --hidden"

"" An action can be a reference to a function that processes selected lines
"function! s:build_quickfix_list(lines)
"  call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
"  copen
"  cc
"endfunction

"let g:fzf_action = {
"  \ 'ctrl-q': function('s:build_quickfix_list'),
"  \ 'ctrl-t': 'tab split',
"  \ 'ctrl-x': 'split',
"  \ 'ctrl-v': 'vsplit' }

"" Border color
""let g:fzf_layout = {'up':'~90%', 'window': { 'width': 0.8, 'height': 0.8,'yoffset':0.5,'xoffset': 0.5, 'highlight': 'Todo', 'border': 'sharp' } }

"" Default fzf layout
"" - Popup window (center of the screen)
""let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } }

"" - Popup window (center of the current window)
""let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6, 'relative': v:true } }

"" - Popup window (anchored to the bottom of the current window)
""let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6, 'relative': v:true, 'yoffset': 1.0 } }

"" - down / up / left / right
"let g:fzf_layout = { 'down': '30%' }

"" - Window using a Vim command
""let g:fzf_layout = { 'window': 'enew' }
""let g:fzf_layout = { 'window': '-tabnew' }
""let g:fzf_layout = { 'window': '10new' }

"" Customize fzf colors to match your color scheme
"" - fzf#wrap translates this to a set of `--color` options
"let g:fzf_colors =
"\ { 'fg':      ['fg', 'Normal'],
"  \ 'bg':      ['bg', 'Normal'],
"  \ 'hl':      ['fg', 'Comment'],
"  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
"  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
"  \ 'hl+':     ['fg', 'Statement'],
"  \ 'info':    ['fg', 'PreProc'],
"  \ 'border':  ['fg', 'Ignore'],
"  \ 'prompt':  ['fg', 'Conditional'],
"  \ 'pointer': ['fg', 'Exception'],
"  \ 'marker':  ['fg', 'Keyword'],
"  \ 'spinner': ['fg', 'Label'],
"  \ 'header':  ['fg', 'Comment'] }

"""Get Files
""command! -bang -nargs=? -complete=dir Files
"    "\ call fzf#vim#files(<q-args>, fzf#vim#with_preview({'options': ['--layout=reverse', '--info=inline']}), <bang>0)


""" Get text in files with Rg
""command! -bang -nargs=* Rg
"  "\ call fzf#vim#grep(
"  "\   'rg --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
"  "\   fzf#vim#with_preview(), <bang>0)

""" Ripgrep advanced
""function! RipgrepFzf(query, fullscreen)
"  "let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case %s || true'
"  "let initial_command = printf(command_fmt, shellescape(a:query))
"  "let reload_command = printf(command_fmt, '{q}')
"  "let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
"  "call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
""endfunction

""command! -nargs=* -bang RG call RipgrepFzf(<q-args>, <bang>0)

""" Git grep
""command! -bang -nargs=* GGrep
"  "\ call fzf#vim#grep(
"  "\   'git grep --line-number '.shellescape(<q-args>), 0,
"  "\   fzf#vim#with_preview({'dir': systemlist('git rev-parse --show-toplevel')[0]}), <bang>0)

let g:previm_open_cmd = 'brave'
