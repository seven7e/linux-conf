set nocompatible	" Use Vim defaults (much better!)
"set nocp
set showmatch               " show matching
"set ignorecase              " case insensitive
"set mouse=v                 " middle-click paste with
set hlsearch                " highlight search
set incsearch               " incremental search

set tabstop=4               " number of columns occupied by a tab
set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing
set expandtab               " converts tabs to white space
set shiftwidth=4            " width for autoindents
set autoindent              " indent a new line the same amount as the line just typed
autocmd FileType yaml,html,javascript,javascriptreact,typescript,typescriptreact,purescript setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
set foldlevel=20
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
" setlocal foldmethod=indent
" set nofoldenable

set number                  " add line numbers
set wildmode=longest,list   " get bash-like tab completions
set cc=80                   " set an 80 column border for good coding style
"set mouse=a                 " enable mouse click
"set clipboard=unnamedplus   " using system clipboard
set cursorline              " highlight current cursorline
set ttyfast                 " Speed up scrolling in Vim
" set spell                 " enable spell check (may need to download language package)
" set noswapfile            " disable creating swap file
" set backupdir=~/.cache/vim " Directory to store backup files.
set cindent
syntax enable
syntax on                   " syntax highlighting
filetype on
filetype plugin indent on   "allow auto-indenting depending on file type
filetype plugin on
set completeopt=longest,menu
set laststatus=2
exec 'set statusline=No\.%n:\ [%f%R]\ %l/%L,\ %c,%V%8P\ %{&fenc}%y'
set bs=indent,eol,start		" allow backspacing over everything in insert mode
set ai			" always set autoindenting on
"set backup		" keep a backup file
set viminfo='20,\"50	" read/write a .viminfo file, don't store more
" than 50 lines of registers
set history=100		" keep 50 lines of command line history
"set ruler		" show the cursor position all the time
let mapleader = ","

if v:lang =~ "utf8$" || v:lang =~ "UTF-8$"
    set fileencodings=utf-8,latin1
endif

"nmap <C-_> <leader>c<Space>
"vmap <C-_> <leader>c<Space>

augroup HighlightWhenYank
    autocmd!
    autocmd TextYankPost * silent! lua vim.highlight.on_yank {timeout=500}
augroup END

augroup redhat
    autocmd!
    " In text files, always limit the width of text to 78 characters
    " autocmd BufRead *.txt set tw=78
    " When editing a file, always jump to the last cursor position
    autocmd BufReadPost *
                \ if line("'\"") > 0 && line ("'\"") <= line("$") |
                \   exe "normal! g'\"" |
                \ endif
augroup END

augroup FormatOptions
    autocmd!
    autocmd VimEnter * setlocal formatoptions-=ro
    autocmd BufEnter * setlocal formatoptions-=ro
augroup END

" if has("cscope") && filereadable("/usr/bin/cscope")
"    set csprg=/usr/bin/cscope
"    set csto=0
"    set cst
"    set nocsverb
"    " add any database in current directory
"    if filereadable("cscope.out")
"       cs add cscope.out
"    " else add database pointed to by environment
"    elseif $CSCOPE_DB != ""
"       cs add $CSCOPE_DB
"    endif
"    set csverb
" endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
    syntax on
    set hlsearch
endif

if &term=="xterm"
    set t_Co=8
    set t_Sb=[4%dm
    set t_Sf=[3%dm
endif

augroup FiletypeDetection
    au!
    " au BufNewFile,BufRead *.pig set filetype=pig syntax=pig
    au BufNewFile,BufRead *.Xmodmap.* set filetype=xmodmap syntax=xmodmap
    au BufNewFile,BufRead *.shrc set filetype=sh syntax=sh
    au BufNewFile,BufRead *.shrc.* set filetype=sh syntax=sh
augroup END


" strip trailing whitespace before saving
fun! StripTrailingWhitespace()
    " Don't strip on these filetypes
    if &ft =~ 'markdown'
        return
    endif
    %s/\s\+$//e
endfun
autocmd BufWritePre * call StripTrailingWhitespace()

" hi CursorLine   cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white


" let g:slimv_swank_cmd='! xterm -e sbcl --load ~/.vim/slime/start-swank.lisp &'


