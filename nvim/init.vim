source $HOME/.config/nvim/base.vim
source $HOME/.config/nvim/plugins.vim
source $HOME/.config/nvim/langs.vim

lua require('base')
lua require('plugins')

source $HOME/.config/nvim/keys.vim
