local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    { 'dstein64/vim-startuptime' },

    ---- langs ------
    { 'purescript-contrib/purescript-vim' },
    { 'scrooloose/vim-slumlord' },
    { 'aklt/plantuml-syntax' },
    'weirongxu/plantuml-previewer.vim',
    'tyru/open-browser.vim' ,

    ---- themes ----
    { 'folke/tokyonight.nvim' },
    { 'morhetz/gruvbox' },
    { 'navarasu/onedark.nvim' },
    { 'rakr/vim-one' },
    -- { 'catppuccin/nvim', as = 'catppuccin' },
    { 'jnurmine/Zenburn' },

    { 'tpope/vim-repeat' },
    { 'kyazdani42/nvim-tree.lua',
        config = function() require'nvim-tree'.setup {} end
    },
    { 'kyazdani42/nvim-web-devicons' }, -- optional, for file icon,
    { 'phaazon/hop.nvim',
        branch = 'v1', -- optional but strongly recommended
        config = function()
            -- you can configure Hop the way you like here; see :h hop-config
            require'hop'.setup { keys = 'etsagkjfudrwlqzmxpcvbhnoi' }
        end
    },
    { 'ggandor/lightspeed.nvim' },
    { "folke/which-key.nvim",
        config = function() require("which-key").setup {} end
    },
    { 'tpope/vim-commentary' },
    'nvim-telescope/telescope.nvim',
    'nvim-lua/plenary.nvim',

    { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
    { 'mg979/vim-visual-multi' },
    -- 'vim-airline/vim-airline',
    'nvim-lualine/lualine.nvim',

    -- { 'tpope/vim-surround' },
    { 'machakann/vim-sandwich' },
    { 'jiangmiao/auto-pairs' },
    ---- Load on an autocommand event
    { 'andymass/vim-matchup' }, -- , event = 'VimEnter' },
    { 'akinsho/toggleterm.nvim' },
    { 'tpope/vim-fugitive' },
    { 'tpope/vim-sleuth' },
    { 'kshenoy/vim-signature' },
    'lewis6991/gitsigns.nvim',

    -------------- lsp plugins -----------------
    -- { 'williamboman/nvim-lsp-installer', requires = { 'neovim/nvim-lspconfig' } },
    { "williamboman/mason.nvim",
        build = ":MasonUpdate" -- :MasonUpdate updates registry contents
    },
    "williamboman/mason-lspconfig.nvim",
    "neovim/nvim-lspconfig",
    { 'j-hui/fidget.nvim' }, -- show lsp process
    { 'nvim-treesitter/nvim-treesitter', build = ':TSUpdate' },
    --, requires = { { "p00f/nvim-ts-rainbow" }, { "windwp/nvim-ts-autotag" } } }

    'hrsh7th/nvim-cmp',
    { 'hrsh7th/cmp-nvim-lsp' },
    { 'hrsh7th/cmp-path' },
    { 'hrsh7th/cmp-cmdline' },
    { 'hrsh7th/cmp-buffer' },
    { 'L3MON4D3/LuaSnip' },
    { 'saadparwaiz1/cmp_luasnip' },

    { 'hrsh7th/cmp-nvim-lsp-signature-help' },
    { 'onsails/lspkind-nvim' },
    -- { 'liuchengxu/vista.vim' },
    { 'simrat39/symbols-outline.nvim' },

    { 'simrat39/rust-tools.nvim' },
    ---- You can specify rocks in isolation
    --use_rocks 'penlight'
    --use_rocks {'lua-resty-http', 'lpeg'}

    ---- Post-install/update hook with call of vimscript function with argument
    --use { 'glacambre/firenvim', run = function() vim.fn['firenvim#install'](0) end }

    { 'Vigemus/iron.nvim' },

    -- 'quarto-dev/quarto-nvim',
    -- 'jmbuhr/otter.nvim',
    -- { 'codota/tabnine-nvim', build = "./dl_binaries.sh" },

    {
        "yetone/avante.nvim",
        event = "VeryLazy",
        lazy = false,
        version = false, -- set this if you want to always pull the latest change
        opts = {
            -- add any opts here
            hints = { enabled = true },
        },
        -- if you want to build from source then do `make BUILD_FROM_SOURCE=true`
        build = "make",
        -- build = "powershell -ExecutionPolicy Bypass -File Build.ps1 -BuildFromSource false" -- for windows
        dependencies = {
            "nvim-treesitter/nvim-treesitter",
            "stevearc/dressing.nvim",
            "nvim-lua/plenary.nvim",
            "MunifTanjim/nui.nvim",
            --- The below dependencies are optional,
            "nvim-tree/nvim-web-devicons", -- or echasnovski/mini.icons
            -- "zbirenbaum/copilot.lua", -- for providers='copilot'
            {
                -- support for image pasting
                "HakonHarnes/img-clip.nvim",
                event = "VeryLazy",
                opts = {
                    -- recommended settings
                    default = {
                        embed_image_as_base64 = false,
                        prompt_for_file_name = false,
                        drag_and_drop = {
                            insert_mode = true,
                        },
                        -- required for Windows users
                        use_absolute_path = true,
                    },
                },
            },
            {
                -- Make sure to set this up properly if you have lazy=true
                'MeanderingProgrammer/render-markdown.nvim',
                opts = {
                    file_types = { "markdown", "Avante" },
                },
                ft = { "markdown", "Avante" },
            },
        },
    }

    --
}, {})

-- ----- set theme -----
require('onedark').setup {
    -- Main options --
    style = 'darker', -- Default theme style. Choose between 'dark', 'darker', 'cool', 'deep', 'warm', 'warmer' and 'light'
    -- transparent = false,  -- Show/hide background
    -- term_colors = true, -- Change terminal color as per the selected theme style
    -- ending_tildes = false, -- Show the end-of-buffer tildes. By default they are hidden
    -- toggle theme style ---
    toggle_style_key = '<leader>ts', -- Default keybinding to toggle
    -- toggle_style_list = {'dark', 'darker', 'cool', 'deep', 'warm', 'warmer', 'light'}, -- List of styles to toggle between

    -- Change code style ---
    -- Options are italic, bold, underline, none
    -- You can configure multiple style with comma seperated, For e.g., keywords = 'italic,bold'
    -- code_style = {
    --     comments = 'italic',
    --     keywords = 'none',
    --     functions = 'none',
    --     strings = 'none',
    --     variables = 'none'
    -- },

    -- Custom Highlights --
    -- colors = {}, -- Override default colors
    -- highlights = {}, -- Override highlight groups

    -- Plugins Config --
    -- diagnostics = {
    --     darker = true, -- darker colors for diagnostic
    --     undercurl = true,   -- use undercurl instead of underline for diagnostics
    --     background = true,    -- use background color for virtual text
    -- },
}
require('onedark').load()

--vim.cmd[[colorscheme tokyonight]]
--vim.cmd[[colorscheme gruvbox]]
-- vim.cmd[[colorscheme catppuccin]]
--vim.cmd[[colorscheme one]]
--vim.opt.background = 'dark'
--vim.g.one_allow_italics = 1
-- require("catppuccin").setup {
--     -- transparent_background = false,
--     -- term_colors = false,
--     -- styles = {
--     --     comments = "italic",
--     --     functions = "italic",
--     --     keywords = "italic",
--     --     strings = "NONE",
--     --     variables = "italic",
--     -- },
--     integrations = {
--         treesitter = true,
--         native_lsp = {
--             enabled = true,
--             -- virtual_text = {
--             --     errors = "italic",
--             --     hints = "italic",
--             --     warnings = "italic",
--             --     information = "italic",
--             -- },
--             -- underlines = {
--             --     errors = "underline",
--             --     hints = "underline",
--             --     warnings = "underline",
--             --     information = "underline",
--             -- },
--         },
--         -- lsp_trouble = false,
--         cmp = true,
--         lsp_saga = false,
--         gitgutter = false,
--         gitsigns = false,
--         telescope = true,
--         nvimtree = {
--             enabled = true,
--             -- show_root = false,
--             -- transparent_panel = false,
--         },
--         neotree = {
--             enabled = false,
--             -- show_root = false,
--             -- transparent_panel = false,
--         },
--         which_key = true,
--         indent_blankline = {
--             enabled = false,
--             -- colored_indent_levels = false,
--         },
--         dashboard = true,
--         neogit = false,
--         vim_sneak = false,
--         fern = false,
--         barbar = false,
--         bufferline = false,
--         markdown = true,
--         lightspeed = false,
--         ts_rainbow = false,
--         hop = true,
--         notify = false,
--         telekasten = false,
--     }
-- }

require('lualine').setup {
    options = {
        theme = 'onedark',
        -- theme = require'lualine.themes.powerline',
        component_separators = { left = '', right = ''},
        section_separators = { left = '', right = ''},
    }
}

----- key bindings -----
local utils = require('utils')
local map = utils.map
-- local options = { noremap = false, silent = false }

-------------- surround -------------
-- use 'o' key instead of 's'
-- vim.g.surround_no_mappings = true
-- map('n', 'do',  '<Plug>Dsurround', options)
-- map('n', 'co',  '<Plug>Csurround', options)
-- map('n', 'cO', '<Plug>CSurround', options)
-- map('n', 'yo', '<Plug>Ysurround', options)
-- map('n', 'yO', '<Plug>YSurround', options)
-- map('n', 'yoo', '<Plug>Yssurround', options)
-- map('n', 'yOo', '<Plug>YSsurround', options)
-- map('n', 'yOO', '<Plug>YSsurround', options)
-- map('v', 'o',   '<Plug>VSurround', options)
-- map('v', 'go', '<Plug>VgSurround', options)

----------------- vim-sandwich -----------------
vim.g.operator_sandwich_no_default_key_mappings = true
vim.g.textobj_sandwich_no_default_key_mappings = true
local opt_re = { noremap = false }
-- add
map('n', 'ms', '<Plug>(sandwich-add)', opt_re)
map('x', 'ms', '<Plug>(sandwich-add)', opt_re)
-- map('o', 'yo', '<Plug>(sandwich-add)', opt_re)
-- delete
map('n', 'ds', '<Plug>(sandwich-delete)', opt_re)
-- map('x', 'ds', '<Plug>(sandwich-delete)', opt_re)
map('n', 'dsa', '<Plug>(sandwich-delete-auto)', opt_re)
-- replace
map('n', 'cs', '<Plug>(sandwich-replace)', opt_re)
-- map('x', 'cs', '<Plug>(sandwich-replace)', opt_re)
map('n', 'csa', '<Plug>(sandwich-replace-auto)', opt_re)

-- text objects
-- auto
map('o', 'ia', '<Plug>(textobj-sandwich-auto-i)')
map('x', 'ia', '<Plug>(textobj-sandwich-auto-i)')
map('o', 'aa', '<Plug>(textobj-sandwich-auto-a)')
map('x', 'aa', '<Plug>(textobj-sandwich-auto-a)')
-- query
map('o', 'is', '<Plug>(textobj-sandwich-query-i)')
map('x', 'is', '<Plug>(textobj-sandwich-query-i)')
map('o', 'as', '<Plug>(textobj-sandwich-query-a)')
map('x', 'as', '<Plug>(textobj-sandwich-query-a)')

-- change highlights to fint onedark
vim.api.nvim_set_hl(0, 'OperatorSandwichBuns', { link = 'PmenuSel' })  -- bg blue
-- vim.highlight.link('OperatorSandwichChange', 'PmenuSel')  -- bg blue
-- vim.highlight.link('OperatorSandwichDelete', 'PmenuSel')  -- bg blue
-- vim.highlight.link('OperatorSandwichAdd', 'PmenuSel')  -- bg blue
vim.fn['operator#sandwich#set']('all', 'all', 'highlight', 1)

---------- vim-commentary ----------
map('n', '<C-_>', '<Plug>CommentaryLine', { noremap = false } )
map('x', '<C-_>', '<Plug>Commentary', { noremap = false } )
map('i', '<C-_>', '<Esc><Plug>CommentaryLineA', { noremap = false } )

----- nvim tree  -----
map('n', '<leader>n', ':NvimTreeToggle<CR>')
map('n', '<leader>r', ':NvimTreeRefresh<CR>')
map('n', '<leader>f', ':NvimTreeFindFile<CR>')

---------------- vista ---------------
-- map('n', '<leader>o', ':Vista!!<CR>')
------------- symbols outline -----------------
map('n', '<leader>o', ':SymbolsOutline<CR>')

---------------- Hop ----------------
--map('', 's', '<cmd>HopChar1<CR>')
--map('o', 's', 'v<cmd>HopChar1<CR>')
-- map('n', 's', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true })<cr>")
-- map('n', 'S', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true })<cr>")
-- map('v', 's', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true })<cr>")
-- map('v', 'S', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true })<cr>")
map('n', 't', "<cmd>lua require'hop'.hint_char1()<cr>")
map('v', 't', "<cmd>lua require'hop'.hint_char1()<cr>")
map('n', 'T', "<cmd>lua require'hop'.hint_char2()<cr>")
map('v', 'T', "<cmd>lua require'hop'.hint_char2()<cr>")
-- map('n', 'T', "<cmd>lua require'hop'.hint_char1({ current_line_only = true, inclusive_jump = true })<cr>")
-- map('n', '<leader>T', "<cmd>lua require'hop'.hint_char2()<cr>")
-- map('v', '<leader>T', "<cmd>lua require'hop'.hint_char2()<cr>")
map('n', '<leader>l', "<cmd>lua require'hop'.hint_lines()<cr>")
map('v', '<leader>l', "<cmd>lua require'hop'.hint_lines()<cr>")
map('n', '<leader>w', "<cmd>lua require'hop'.hint_words()<cr>")
map('v', '<leader>w', "<cmd>lua require'hop'.hint_words()<cr>")
-- map('o', 't', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true, inclusive_jump = true })<cr>")
-- map('o', 'T', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true, inclusive_jump = true })<cr>")

--------------- toggleterm ----------------
require("toggleterm").setup{
    -- size can be a number or function which is passed the current terminal
    -- size = 20 | function(term)
    --     if term.direction == "horizontal" then
    --         return 15
    --     elseif term.direction == "vertical" then
    --         return vim.o.columns * 0.4
    --     end
    -- end,
    open_mapping = [[<c-\>]],
    -- on_open = fun(t: Terminal), -- function to run when the terminal opens
    -- on_close = fun(t: Terminal), -- function to run when the terminal closes
    -- hide_numbers = true, -- hide the number column in toggleterm buffers
    -- shade_filetypes = {},
    -- shade_terminals = true,
    -- shading_factor = '<number>', -- the degree by which to darken to terminal colour, default: 1 for dark backgrounds, 3 for light
    -- start_in_insert = true,
    -- insert_mappings = true, -- whether or not the open mapping applies in insert mode
    -- terminal_mappings = true, -- whether or not the open mapping applies in the opened terminals
    -- persist_size = true,
    -- direction = 'vertical' | 'horizontal' | 'window' | 'float',
    direction = 'float',
    close_on_exit = true, -- close the terminal window when the process exits
    -- shell = vim.o.shell, -- change the default shell
    -- This field is only relevant if direction is set to 'float'
    -- float_opts = {
    --     -- The border key is *almost* the same as 'nvim_open_win'
    --     -- see :h nvim_open_win for details on borders however
    --     -- the 'curved' border is a custom border type
    --     -- not natively supported but implemented in this plugin.
    --     border = 'single' | 'double' | 'shadow' | 'curved' | ... other options supported by win open
    --     width = <value>,
    --     height = <value>,
    --     winblend = 3,
    --     highlights = {
    --         border = "Normal",
    --         background = "Normal",
    --     }
    -- }
}

--------------- telescope -----------------
local telescope = require('telescope')
telescope.setup {}
telescope.load_extension('fzf')

-- Find files using Telescope command-line sugar.
map('n', '<C-p>', '<cmd>Telescope git_files<cr>')
map('n', '<leader>tf', '<cmd>Telescope find_files<cr>')
map('n', '<leader>e', '<cmd>Telescope grep_string<cr>')
map('n', '<leader>g', '<cmd>Telescope live_grep<cr>')
map('n', '<leader>b', '<cmd>Telescope buffers<cr>')
map('n', '<leader>th', '<cmd>Telescope help_tags<cr>')
map('n', '<leader>td', '<cmd>Telescope lsp_definitions<cr>')
map('n', '<leader>tr', '<cmd>Telescope lsp_references<cr>')
map('n', '<leader>ti', '<cmd>Telescope lsp_implementations<cr>')
-- map('n', '<leader>ta', '<cmd>Telescope lsp_code_actions<cr>')

----------------- gitsigns ----------------------
require('gitsigns').setup {
    on_attach = function(bufnr)
        local function _map(mode, lhs, rhs, opts)
            opts = vim.tbl_extend('force', {noremap = true, silent = true}, opts or {})
            vim.api.nvim_buf_set_keymap(bufnr, mode, lhs, rhs, opts)
        end

        -- Navigation
        _map('n', 'gh', "&diff ? ']c' : '<cmd>Gitsigns next_hunk<CR>'", {expr=true})
        _map('n', 'gH', "&diff ? '[c' : '<cmd>Gitsigns prev_hunk<CR>'", {expr=true})
        _map('n', ']c', "&diff ? ']c' : '<cmd>Gitsigns next_hunk<CR>'", {expr=true})
        _map('n', '[c', "&diff ? '[c' : '<cmd>Gitsigns prev_hunk<CR>'", {expr=true})


        -- Actions
        _map('n', '<leader>hs', ':Gitsigns stage_hunk<CR>')
        _map('v', '<leader>hs', ':Gitsigns stage_hunk<CR>')
        _map('n', '<leader>hr', ':Gitsigns reset_hunk<CR>')
        _map('v', '<leader>hr', ':Gitsigns reset_hunk<CR>')
        _map('n', '<leader>hS', '<cmd>Gitsigns stage_buffer<CR>')
        _map('n', '<leader>hu', '<cmd>Gitsigns undo_stage_hunk<CR>')
        _map('n', '<leader>hR', '<cmd>Gitsigns reset_buffer<CR>')
        _map('n', '<leader>hp', '<cmd>Gitsigns preview_hunk<CR>')
        _map('n', '<leader>hb', '<cmd>lua require"gitsigns".blame_line{full=true}<CR>')
        _map('n', '<leader>htb', '<cmd>Gitsigns toggle_current_line_blame<CR>')
        _map('n', '<leader>hd', '<cmd>Gitsigns diffthis<CR>')
        _map('n', '<leader>hD', '<cmd>lua require"gitsigns".diffthis("~")<CR>')
        _map('n', '<leader>htd', '<cmd>Gitsigns toggle_deleted<CR>')

        -- Text object
        _map('o', 'ih', ':<C-U>Gitsigns select_hunk<CR>')
        _map('x', 'ih', ':<C-U>Gitsigns select_hunk<CR>')
    end
}

--------------- Setup nvim-cmp.-----------------
local cmp = require'cmp'
local lspkind = require('lspkind')

cmp.setup {
    formatting = {
        format = lspkind.cmp_format {
            mode = 'symbol_text', -- show only symbol annotations
            maxwidth = 50, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
            menu = {
                buffer = "[Buffer]",
                nvim_lsp = "[LSP]",
                luasnip = "[LuaSnip]",
                nvim_lua = "[Lua]",
                latex_symbols = "[Latex]",
            },
        },
    },
    snippet = {
        -- REQUIRED - you must specify a snippet engine
        expand = function(args)
            require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        end,
    },
    mapping = {
        ['<C-u>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
        ['<C-d>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
        ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
        -- ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
        -- ['<C-e>'] = cmp.mapping({ i = cmp.mapping.abort(), c = cmp.mapping.close(), }),
        ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    },
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'luasnip' }, -- For luasnip users.
        { name = 'nvim_lsp_signature_help' },
    }, {
        { name = 'buffer' },
        { name = "path" },
    }),
    experimental = {
        ghost_text = true,
    },
}

-- Set configuration for specific filetype.
cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources({
        { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
    }, {
        { name = 'buffer' },
    })
})

-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline('/', {
    sources = {
        { name = 'buffer' }
    }
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
    sources = cmp.config.sources({
        { name = 'path' }
    }, {
        { name = 'cmdline' }
    })
})

-- Setup lspconfig.
local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

-------------------- LSP -----------------------
-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
--map('n', '<space>e', '<cmd>lua vim.diagnostic.open_float()<CR>')
map('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>')
map('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>')
--map('n', '<space>q', '<cmd>lua vim.diagnostic.setloclist()<CR>')

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
--local lsp_on_attach = function(client, bufnr)
--    -- Enable completion triggered by <c-x><c-o>
--    -- omnifunc does not work with nvim-cmp
--    -- vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

--    -- Mappings.
--    local bufmap = utils.bufmap
--    -- See `:help vim.lsp.*` for documentation on any of the below functions
--    bufmap(bufnr, 'n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>')
--    bufmap(bufnr, 'n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>')
--    bufmap(bufnr, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>')
--    bufmap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>')
--    --bufmap(bufnr, 'n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>')
--    --bufmap(bufnr, 'n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>')
--    --bufmap(bufnr, 'n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>')
--    --bufmap(bufnr, 'n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>')
--    --bufmap(bufnr, 'n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>')
--    bufmap(bufnr, 'n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>')
--    bufmap(bufnr, 'n', '<leader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>')
--    bufmap(bufnr, 'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>')
--    --bufmap(bufnr, 'n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>')
--end

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { buffer = ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    -- vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
    -- vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
    -- vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
    -- vim.keymap.set('n', '<space>wl', function()
    --   print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    -- end, opts)
    -- vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, opts)
    vim.keymap.set({ 'n', 'v' }, '<leader>ca', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    -- vim.keymap.set('n', '<space>f', function()
    --   vim.lsp.buf.format { async = true }
    -- end, opts)
  end,
})

------------ LSP Installer --------------------
-- local lsp_installer = require("nvim-lsp-installer")

-- -- Register a handler that will be called for each installed server when it's ready (i.e. when installation is finished
-- -- or if the server is already installed).
-- lsp_installer.on_server_ready(function(server)
--     local opts = {
--         on_attach = lsp_on_attach,
--         capabilities = capabilities,
--     }

--     -- (optional) Customize the options passed to the server
--     -- if server.name == "tsserver" then
--     --     opts.root_dir = function() ... end
--     -- end

--     -- This setup() function will take the provided server configuration and decorate it with the necessary properties
--     -- before passing it onwards to lspconfig.
--     -- Refer to https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
--     server:setup(opts)
-- end)

-- lsp_installer.settings({
--     ui = {
--         icons = {
--             server_installed = "✓",
--             server_pending = "➜",
--             server_uninstalled = "✗"
--         }
--     }
-- })

local handlers = {
    -- The first entry (without a key) will be the default handler
    -- and will be called for each installed server that doesn't have
    -- a dedicated handler.
    function (server_name) -- default handler (optional)
        require("lspconfig")[server_name].setup {
            -- on_attach = lsp_on_attach,
            capabilities = capabilities,
        }
    end,
    -- Next, you can provide targeted overrides for specific servers.
    ["rust_analyzer"] = function ()
        require("rust-tools").setup {
            tools = {
                hover_actions = {
                    -- whether the hover action window gets automatically focused
                    auto_focus = false,
                },
            },
            server = {
                on_attach = function(_, bufnr)
                    -- require("dap")
                    -- require("dapui")
                    -- Hover actions
                    -- vim.keymap.set("n", "K", rt.hover_actions.hover_actions, { buffer = bufnr })
                    -- Code action groups
                    -- vim.keymap.set("n", "<Space>a", rt.code_action_group.code_action_group, { buffer = bufnr })
                end,
                flags = {
                    debounce_text_changes = 150,
                },
                settings = {
                    ["rust-analyzer"] = {
                        checkOnSave = {
                            enable = true,
                            command = "clippy",
                        },
                        cargo = {
                            allFeatures = true,
                        },
                    },
                },
            },
        }
    end,
    -- ["lua_ls"] = function ()
    --     lspconfig.lua_ls.setup {
    --         settings = {
    --             Lua = {
    --                 diagnostics = {
    --                     globals = { "vim" }
    --                 }
    --             }
    --         }
    --     }
    -- end,
}

require("mason").setup()
require("mason-lspconfig").setup {
    ensure_installed = { "lua_ls", "rust_analyzer" },
    handlers = handlers,
}

-------------- treesitter ---------------
require'nvim-treesitter.configs'.setup {
    ensure_installed = {
        "c",
        "lua",
        "rust",
        "javascript",
        "solidity",
        "typescript",
        "json",
        "python",
        "markdown",
        "go",
    },
    auto_install = true,
    highlight = {
        -- `false` will disable the whole extension
        enable = true,
    },
    matchup = {
        enable = true,              -- mandatory, false will disable the whole extension
        disable = { "markdown" },  -- optional, list of language that will be disabled
    },
}

require"fidget".setup{} -- show lsp progress

---------- iron for REPL -----------------
local iron = require("iron.core")
local iron_view = require("iron.view")

iron.setup {
  config = {
    -- Whether a repl should be discarded or not
    scratch_repl = true,
    -- Your repl definitions come here
    repl_definition = {
      sh = {
        -- Can be a table or a function that
        -- returns a table (see below)
        command = {"zsh"}
      },
      python = require("iron.fts.python").ipython,
    },
    -- How the repl window will be displayed
    -- See below for more information
    -- repl_open_cmd = iron_view.bottom(40),
    repl_open_cmd = iron_view.split.vertical.botright(0.5),
  },
  -- Iron doesn't set keymaps by default anymore.
  -- You can set them here or manually add keymaps to the functions in iron.core
  keymaps = {
    send_motion = "<leader>sc",
    visual_send = "<leader>sc",
    -- send_file = "<leader>sf",
    send_line = "<leader>sl",
    send_until_cursor = "<leader>su",
    send_mark = "<leader>sm",
    -- mark_motion = "<leader>mc",
    -- mark_visual = "<leader>mc",
    remove_mark = "<leader>md",
    cr = "<leader>s<cr>",
    -- interrupt = "<leader>s<space>",
    -- exit = "<leader>sq",
    -- clear = "<leader>scl",
  },
  -- If the highlight is on, you can change how it looks
  -- For the available options, check nvim_set_hl
  highlight = {
    italic = true,
    bold = true,
    -- undercurl = true,
    -- underdouble = true,
  },
  ignore_blank_lines = true, -- ignore blank lines when sending visual select lines
}

-- iron also has a list of commands, see :h iron-commands for all available commands
vim.keymap.set('n', '<leader>ss', '<cmd>IronRepl<cr>')
-- vim.keymap.set('n', '<leader>sr', '<cmd>IronRestart<cr>')
-- vim.keymap.set('n', '<leader>sf', '<cmd>IronFocus<cr>')
-- vim.keymap.set('n', '<leader>sh', '<cmd>IronHide<cr>')
map('n', '<leader>x', '<leader>sc$', {desc = 'Execute code at cursor till EOL', noremap = false})

---------------- Quarto ---------------------------
-- local quarto = require('quarto')

-- quarto.setup{
--   debug = false,
--   closePreviewOnExit = true,
--   lspFeatures = {
--     enabled = true,
--     chunks = "curly",
--     -- languages = { "r", "python", "julia", "bash", "html" },
--     languages = { "python" },
--     diagnostics = {
--       enabled = true,
--       triggers = { "BufWritePost" },
--     },
--     completion = {
--       enabled = true,
--     },
--   },
--   codeRunner = {
--     enabled = false,
--     default_method = nil, -- 'molten' or 'slime'
--     ft_runners = {}, -- filetype to runner, ie. `{ python = "molten" }`.
--                      -- Takes precedence over `default_method`
--     never_run = { "yaml" }, -- filetypes which are never sent to a code runner
--   },
--   keymap = {
--     -- set whole section or individual keys to `false` to disable
--     hover = "K",
--     definition = "gd",
--     type_definition = "gD",
--     -- rename = "<leader>lR",
--     rename = "<leader>rn",
--     format = "<leader>lf",
--     references = "gr",
--     document_symbols = "gS",
--   }
-- }

-- vim.keymap.set('n', '<leader>qp', quarto.quartoPreview, { silent = true, noremap = true })

-- local runner = require("quarto.runner")
-- vim.keymap.set("n", "<leader>qc", runner.run_cell,  { desc = "run cell", silent = true })
-- vim.keymap.set("n", "<leader>qu", runner.run_above, { desc = "run until cursor", silent = true })
-- vim.keymap.set("n", "<leader>qa", runner.run_all,   { desc = "run all cells", silent = true })
-- vim.keymap.set("n", "<leader>ql", runner.run_line,  { desc = "run line", silent = true })
-- vim.keymap.set("v", "<leader>q",  runner.run_range, { desc = "run visual range", silent = true })
-- vim.keymap.set("n", "<leader>QA", function()
--   runner.run_all(true)
-- end, { desc = "run all cells of all languages", silent = true })

---------------- tabnine ---------------------
-- require('tabnine').setup({
--   disable_auto_comment=true,
--   accept_keymap="<Tab>",
--   dismiss_keymap = "<C-]>",
--   debounce_ms = 800,
--   suggestion_color = {gui = "#808080", cterm = 244},
--   exclude_filetypes = {"TelescopePrompt", "NvimTree"},
--   log_file_path = nil, -- absolute path to Tabnine log file
-- })
