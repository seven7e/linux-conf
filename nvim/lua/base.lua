-- format rust yew code inside html!
vim.api.nvim_set_keymap( 'n', '<leader>py', 'vi{:! prettier --parser html --stdin-filepath<CR>vi{>', {noremap = true})
