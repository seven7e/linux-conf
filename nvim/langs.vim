function HeaderPython()
    call setline(1, '#!/usr/bin/env python')
    call append(1, '# -*- coding: utf-8 -*-')
    call append(2, '')
    call append(3, 'import sys')
    call append(4, '')
    call append(5, '')
    call append(6, "def main():")
    call append(7, "    return")
    call append(8, '')
    call append(9, '')
    call append(10, "if __name__ == '__main__':")
    call append(11, "    main()")
    normal G
endf

function HeaderShell()
    call setline(1, '#!/bin/bash')
    call append(2, '')
    normal G
endf

autocmd bufnewfile *.py call HeaderPython()
autocmd bufnewfile *.sh call HeaderShell()


