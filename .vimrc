set nocp
if v:lang =~ "utf8$" || v:lang =~ "UTF-8$"
   set fileencodings=utf-8,latin1
endif

noremap h k
noremap l j
noremap k h
noremap j l

nmap <C-_> <leader>c<Space>
vmap <C-_> <leader>c<Space>

"set nocompatible	" Use Vim defaults (much better!)
set bs=indent,eol,start		" allow backspacing over everything in insert mode
set ai			" always set autoindenting on
"set backup		" keep a backup file
set viminfo='20,\"50	" read/write a .viminfo file, don't store more
			" than 50 lines of registers
set history=50		" keep 50 lines of command line history
"set ruler		" show the cursor position all the time

" Only do this part when compiled with support for autocommands
if has("autocmd")
  augroup redhat
    " In text files, always limit the width of text to 78 characters
    " autocmd BufRead *.txt set tw=78
    " When editing a file, always jump to the last cursor position
    autocmd BufReadPost *
    \ if line("'\"") > 0 && line ("'\"") <= line("$") |
    \   exe "normal! g'\"" |
    \ endif
  augroup END
endif

if has("cscope") && filereadable("/usr/bin/cscope")
   set csprg=/usr/bin/cscope
   set csto=0
   set cst
   set nocsverb
   " add any database in current directory
   if filereadable("cscope.out")
      cs add cscope.out
   " else add database pointed to by environment
   elseif $CSCOPE_DB != ""
      cs add $CSCOPE_DB
   endif
   set csverb
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

if &term=="xterm"
     set t_Co=8
     set t_Sb=[4%dm
     set t_Sf=[3%dm
endif

set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set cindent
syntax enable
syntax on
set showmatch
filetype on
filetype plugin indent on
set completeopt=longest,menu
set laststatus=2
exec 'set statusline=No\.%n:\ [%f%R]\ %l/%L,\ %c,%V%8P\ %{&fenc}%y'

augroup filetypedetect
	au BufNewFile,BufRead *.pig set filetype=pig syntax=pig
augroup END


" strip trailing whitespace before saving
fun! StripTrailingWhitespace()
    " Don't strip on these filetypes
    if &ft =~ 'markdown'
        return
    endif
    %s/\s\+$//e
endfun
autocmd BufWritePre * call StripTrailingWhitespace()

hi CursorLine   cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white


function HeaderPython()
    call setline(1, '#!/usr/bin/env python')
    call append(1, '# -*- coding: utf-8 -*-')
    call append(2, '')
    call append(3, 'import sys')
    call append(4, '')
    call append(5, '')
    call append(6, "def main():")
    call append(7, "    return")
    call append(8, '')
    call append(9, "if __name__ == '__main__':")
    call append(10, "    main()")
    normal G
endf

function HeaderShell()
    call setline(1, '#!/bin/bash')
    call append(2, '')
    normal G
endf

autocmd bufnewfile *.py call HeaderPython()
autocmd bufnewfile *.sh call HeaderShell()

let g:slimv_swank_cmd='! xterm -e sbcl --load ~/.vim/slime/start-swank.lisp &'

silent! call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdcommenter'

call plug#end()
